<?php
echo "<b>ifElse, task5</b> <br><br>";
echo "Написать программу определения оценки студента по его рейтингу, на основе следующих правил<br><br>";

function calcRating($rating){
	$finalresult;
	if ($rating >= 0 && $rating <= 100){
		if ($rating <= 19){
			$finalresult = "F";
		} else if ($rating <= 39){
			$finalresult = "E";
		} else if ($rating <= 59){
			$finalresult = "D";
		} else if ($rating <= 74){
			$finalresult = "C";
		} else if ($rating <= 89){
			$finalresult = "B";
		} else {
			$finalresult = "A";
		}
	} else {
		$finalresult = "Error!";
	};
	return $finalresult;
}

echo "Передаём в функцию 55<br>";
echo calcRating(55);
echo "<br>";
echo "Передаём в функцию 99<br>";
echo calcRating(99);
echo "<br>";
echo "Передаём в функцию 101<br>";
echo calcRating(101);
echo "<br>";


echo '<br><a href="/ifElse">ifElse</a>';
echo '<br><a href="../index.php">Home</a>';
?> 