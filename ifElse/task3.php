<?php
echo "<b>ifElse, task1</b> <br><br>";
echo "Найти суммы только положительных из трех чисел<br><br>";

function addPositive($a, $b, $c){
	if ($a > 0) {
		$res += $a;
	} else {
		$res += 0;
	};

	if ($b > 0) {
		$res += $b;
	} else {
		$res += 0;
	};

	if ($c > 0) {
		$res += $c;
	} else {
		$res += 0;
	};

	return $res;
}

echo "Передаём в функцию числа 1, 2 и 3<br>";
echo addPositive(1, 2, 3);
echo "<br>";
echo "Передаём в функцию числа -3, 3 и 6<br>";
echo addPositive(-3, 3, 6);
echo "<br>";


echo '<br><a href="/ifElse">ifElse</a>';
echo '<br><a href="../index.php">Home</a>';
?> 