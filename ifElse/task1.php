<?php
echo "<b>ifElse, task1</b> <br><br>";
echo "Если а – четное посчитать а*б, иначе а+б<br><br>";

function addOrMult($a, $b){
	if ($a % 2 == 0) {
    	return $a * $b;
	} else {
    	return $a + $b;
    };
};

echo "Передаём в функцию числа 4 и 7<br>";
echo addOrMult(4, 7);
echo "<br>";
echo "Передаём в функцию числа 3 и 6<br>";
echo addOrMult(3, 6);
echo "<br>";

echo '<br><a href="/ifElse">ifElse</a>';
echo '<br><a href="../index.php">Home</a>';
?> 