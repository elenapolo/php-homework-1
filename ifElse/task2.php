<?php
echo "<b>ifElse, task2</b> <br><br>";
echo "Определить какой четверти принадлежит точка с координатами (х,у)<br><br>";
  
function whatQuart($x, $y){
	if ($x > 0 && $y > 0) {
		return "First quarter";
	} else if ($x < 0 && $y > 0) { 
		return "Second quarter";
	} else if ($x < 0 && $y < 0) {
		return "Third quarter";
	} else if ($x > 0 && $y < 0) {
		return "Fourth quarter";
	} else if ($x == 0 || $y == 0) {
		return "Point on axe";
	} else {
		return "Error!";
	} 	
}

echo "Передаём в функцию числа 1 и 2<br>";
echo whatQuart(1, 2);
echo "<br>";
echo "Передаём в функцию числа -3 и -6<br>";
echo whatQuart(-3, -6);
echo "<br>";
echo "Передаём в функцию числа 2 и 0<br>";
echo whatQuart(2, 0);
echo "<br>";


echo '<br><a href="/ifElse">ifElse</a>';
echo '<br><a href="../index.php">Home</a>';
?> 