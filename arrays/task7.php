<?php
echo "<b>arrays, task7</b> <br><br>";
echo "Посчитать количество нечетных элементов массива<br><br>";

echo "Массив [7, -6, 0, 13, 4, 21, -2]<br>";

$arr  = array(7, -6, 0, 13, 4, 21, -2);
$arr2 = [];

foreach ($arr as $value){
    if ($value % 2 != 0){
        array_push($arr2, $value);
    }
};
echo '<br>В массиве '.count($arr2). ' нечетных элементов<br>';

echo '<br><a href="/arrays">arrays</a>';
echo '<br><a href="../index.php">Home</a>';
?> 