<?php
echo "<b>arrays, task9</b> <br><br>";
echo "Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert)) <br><br>";

echo "Массив [7, -6, 0, 13, 4, 21, -2]<br><br>";

$arr  = array(7, -6, 0, 13, 4, 21, -2);
$len = count($arr)-1;

echo 'Bubble sort <br>';

for($i = $len; $i>=0; $i--){
    for($j=0; $j<=($i-1); $j++){
        if($arr[$j]>$arr[$j+1]){
            $temp = $arr[$j];
            $arr[$j] = $arr[$j+1];
            $arr[$j+1] = $temp;
        }
    }    
}
  
echo 'Результат: ';
print_r ($arr);
echo '<br><br>';                    
              
echo 'Select sort<br>';
$arr2 = array(7, -6, 0, 13, 4, 21, -2);
$len = count($arr2);

for($i = 0; $i < $len; $i++) {
    $min = $i;
    for($j = $i + 1; $j < $len; $j++) {
        if($arr2[$j] < $arr2[$min]) $min = $j;
    }
    list($arr2[$i], $arr2[$min]) = array($arr2[$min], $arr2[$i]);
};

echo 'Результат: ';
print_r ($arr2);
echo '<br><br>';  


echo 'Insert sort<br>';

$arr3 = array(7, -6, 0, 13, 4, 21, -2);
$len = count($arr3);

for ($i=1; $i<$len; $i++){
    for($j = $i; $j>0 && $arr3[$j-1]>$arr3[$j];$j--){
        $temp = $arr3[$j-1];
        $arr3[$j-1] = $arr3[$j];
        $arr3[$j] = $temp;
    }
}

echo 'Результат: ';
print_r ($arr3);
echo '<br><br>';  


echo '<br><a href="/arrays">arrays</a>';
echo '<br><a href="../index.php">Home</a>';
?> 