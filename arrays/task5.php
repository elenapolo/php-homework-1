<?php
echo "<b>arrays, task5</b> <br><br>";
echo "Посчитать сумму элементов массива с нечетными индексами<br><br>";


echo "Массив [7, -6, 0, 13, 4, 21, -2]<br>";
$arr = [7, -6, 0, 13, 4, 21, -2];
$odd = array();
$numCnt = count($arr);
 
for($i = 0, $j = 1; $i < $numCnt; $i++, $j++){    
    if($j % 2 != 0)
        $odd[] = $arr[$i];
}
 
echo implode(', ', $odd).'<br>';
echo 'Сумма '.array_sum($odd);
echo "<br>"; 


echo '<br><a href="/arrays">arrays</a>';
echo '<br><a href="../index.php">Home</a>';
?> 