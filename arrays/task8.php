<?php
echo "<b>arrays, task8</b> <br><br>";
echo "Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2<br><br>";

echo "Массив [7, -6, 0, 13, 4, 21, -2]<br>";

$arr  = array(7, -6, 0, 13, 4, 21, -2);
$firstHalf = [];
$secondHalf = [];

list($firstHalf,$secondHalf) = array_chunk($arr, ceil(count($arr)/2));
$result = array_merge($secondHalf, $firstHalf);
echo 'Результат: ';
print_r ($result);

echo '<br><br><a href="/arrays">arrays</a>';
echo '<br><a href="../index.php">Home</a>';
?> 