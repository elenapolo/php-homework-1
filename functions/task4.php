<?php
echo "<b>functions, task4</b> <br><br>";
echo "Найти расстояние между двумя точками в двумерном декартовом пространстве. <br><br>";

function pointDistance($ax, $ay, $bx, $by) {
    $x = $bx - $ax;
    $y = $by - $ay;  
   
    return ((int) hypot($x, $y));
}

echo "Передаём в функцию точки (1.1) и (4.5), получаем результат: ";
echo pointDistance(1, 1, 4, 5);

echo '<br>';
echo '<br><a href="/functions">functions</a>';
echo '<br><a href="../index.php">Home</a>';
?> 