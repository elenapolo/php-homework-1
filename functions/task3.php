<?php
echo "<b>functions, task3</b> <br><br>";
echo "Вводим строку, которая содержит число, написанное прописью (0-999). Получить само число<br><br>";

function writeChislo($num){
    $sotni = ["","сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"];
    $dec = ["", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"];
    $dec2 = ["десять", "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"];
    $edin = ["ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"];
    $answer = 0;
    $string = "";
    $string2 = "";

    $numArr = explode(" ", $num);

    if(count($numArr) == 3){
        $numSotni = $numArr[0];
        $numDec = $numArr[1];
        $numEdin = $numArr[2];
    } else if(count($numArr) == 2){
        $numDec = $numArr[0];
        $numEdin = $numArr[1];
    } else if(count($numArr) == 1){
        $numEdin = $numArr[0];
    } else { 
        return "wrong num";
    };

    if ($numSotni){
        for($i=1; $i<count($sotni); $i++){
            if($numSotni==$sotni[$i]){
                $answer += $i*100;
            }
        }
    }

    if ($numDec){
        for($i=2; $i<count($dec); $i++){
            if($answer>0){
                if($numDec==$dec[$i]){
                    $answer += $i*10;
                }
            } else {
                if($numDec==$dec[$i]){
                    $answer += $i*10;
                }
            }
        }
        for($i=0; $i<count($dec2); $i++){
            if($answer>0){
                if($numDec==$dec2[$i]){
                    $answer += 10+$i;
                }
            } else {
                if($numDec==$dec2[$i]){
                    $answer += 10+$i;
                }
            }
        } 
    }

    if($numEdin){
        for($i=0; $i<count($edin); $i++){
            if ($answer>0){
                if($numEdin==$edin[$i]){
                    $answer += $i;
                }
            } else {
                if($numEdin==$edin[$i]){
                    $answer += $i;
                }
            }
        } 
        return $answer;
    }
}



$task = "триста двадцать два";
echo "Вводим число триста двадцать два, получаем результат: ";
echo writeChislo($task);

print_r($numArr);

echo '<br>';
echo '<br><a href="/functions">functions</a>';
echo '<br><a href="../index.php">Home</a>';
?> 