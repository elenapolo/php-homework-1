<?php
echo "<b>loops, task6</b> <br><br>";
echo "Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.<br><br>";

function reverseNum($a){
	$reverse = 0;
 
	while ($a>0){
							
		$reverse *= 10;
							
		$reverse += ($a%10);
							
		$a = ((int)($a/10));
						
	}
	return $reverse;
}

echo "Передаём в функцию 125<br>";
echo reverseNum(125);
echo "<br>";

echo '<br><a href="/loops">loops</a>';
echo '<br><a href="../index.php">Home</a>';
?> 