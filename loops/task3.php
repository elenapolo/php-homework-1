<?php
echo "<b>loops, task3</b> <br><br>";
echo "Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)<br><br>";

function findKoren($a){              
    $k = 1;		
    while($k < $a){				
		$r = $k * $k;
						
		if ($r == $answ){
			return $k;                                      
		} else if ($r > $a){                        
			return $k - 1;                                        
		} else {
           $k++; 
        }				
	};
};

// Метод бинарного поиска
        
function findKoren2($a){
	$min = 1;
    $max = $a;
	$mid = 0;
	$prev = 0;
	$answer = 1;

	for(;;){			
		$mid = ($min + $max)/2;
					
		if ($prev==$mid){							
			$answer=$mid; 					
			break;					
		}
		
		$d=$mid*$mid;
						
		if ($d == $a){                    
			$answer = $mid;                        
			break;                    
		} else if ($d > $a){                        
			$max = $mid;                    
		} else { 				
			$min = $mid; 				
			$prev = $mid; 			
		}            
	}
	return ((int)$answer);
}                

echo "Передаём в первую функцию 27<br>";
echo findKoren(27);
echo "<br>";
echo "Передаём во вторую функцию 39<br>";
echo findKoren2(39);
echo "<br>";


echo '<br><a href="/loops">loops</a>';
echo '<br><a href="../index.php">Home</a>';
?> 