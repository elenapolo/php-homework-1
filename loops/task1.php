<?php
echo "<b>loops, task1</b> <br><br>";
echo "Найти сумму четных чисел и их количество в диапазоне от 1 до 99<br><br>";

function sumChet($a,$b){
	$suma = 0;
	$amount = 0;
	for($i = $a; $i < $b+1; $i++){
    	if ($i%2 == 0){
			$suma += $i;
			$amount++;
		} else {
			$suma += 0;
		}
	};
	$answer = "Sum is $suma, amount is $amount.";
	return $answer;
};

echo "Передаём в функцию числа 1 и 99<br>";
echo sumChet(1, 99);
echo "<br>";


echo '<br><a href="/loops">loops</a>';
echo '<br><a href="../index.php">Home</a>';
?> 