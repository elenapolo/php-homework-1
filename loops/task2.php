<?php
echo "<b>loops, task2</b> <br><br>";
echo "Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)<br><br>";

function ifSimple($a){
    $q = 0;
	for ($i = 2; $i < $a; $i++){				
		if ($a % $i == 0){					
			$q++;						
		}			
		if($q == 0){					
			return "$a is simple";				
		} else {				
			return "$a is not simple";				
		}
	}
}

echo "Передаём в функцию 27<br>";
echo ifSimple(27);
echo "<br>";
echo "Передаём в функцию 12<br>";
echo ifSimple(12);
echo "<br>";

echo '<br><a href="/loops">loops</a>';
echo '<br><a href="../index.php">Home</a>';
?> 